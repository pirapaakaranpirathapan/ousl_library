import { StyleSheet, Text, View,StatusBar,ActivityIndicator,Button } from 'react-native'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Main from './src/components/Main';
import Login from './src/components/Login';
import Signup from './src/components/Signup';
import { Colors } from './src/contents/Colors';

const Stack = createNativeStackNavigator();


function HomeScreen({navigation}) {
  return (
    <View style={styles.layout}>
      <StatusBar barStyle = "dark-content" hidden = {true}  translucent = {true}/>
      <Text style={{backgroundColor:"white",padding:5,borderRadius:10,color:"red",fontSize:18}}>நூலகத்தைப் பற்றி...</Text>
      <Text style={{textAlign:"justify",padding:15,color:"black"}}>பாறைகளிலும் பனையோலைகளிலும் காகிதங்களிலும் காலங்காலமாக எழுதப்பட்டுவந்த தமிழை மின்னூடகத்திற்கு மாற்றியுள்ளோம். இந்தத் தமிழ் வெள்ளத்தால் போகாது; வெந்தணலால் வேகாது; கடற்கோளால் கொள்ள முடியாது. அச்சுநூற்கள், இதழ்கள், ஓலைச்சுவடிகள், அரிய காகிதச்சுவடிகள், கல்வெட்டுக்கள், நிழற்படங்கள், ஓவியங்கள், சிற்பங்கள், செப்பேடுகள் என தமிழியல் ஆய்வாதாரங்கள் அத்தனையும் இங்கே உள. இதுவரை மறைக்கப்பட்டு வந்த அறிவை உலகமாந்தர் அனைவருக்கும் பொதுமையாக்கும் முயற்சி இது.</Text>
          <ActivityIndicator size="large" color="#00ff00" />
          <View>
            <Button  title='Go To My Library' 
            onPress={()=>navigation.navigate('Main')}
            />

            <View style={styles.minlayout}>
            <Text style={{ backgroundColor: "yellow", flex: 1,color:'black',borderRadius:8}} onPress={()=>navigation.navigate('Login')}> Login</Text>
            <Text style={{ backgroundColor: "white", flex: 0.5 }}> </Text>
            <Text style={{ backgroundColor: "yellow", flex: 1,color:'black',borderRadius:8 }} onPress={()=>navigation.navigate('Signup')}> Signup</Text>
            </View>
          </View>
    </View>
  );
}



const App = () => {
  return (
    <NavigationContainer >
      <Stack.Navigator>
        <Stack.Screen  name="Home" component={HomeScreen} options={{ title: 'Home' }}/>
        <Stack.Screen  name="Main" component={Main} options={{ title: 'Main' }}/>
        <Stack.Screen  name="Login" component={Login} options={{ title: 'Login' }}/>
        <Stack.Screen  name="Signup" component={Signup} options={{ title: 'Signup' }}/>
        
      </Stack.Navigator>
    </NavigationContainer>

  )
}

export default App

const styles = StyleSheet.create({
  layout:{
    backgroundColor:Colors.lightyellow,
    height:"100%",
    flex: 1, 
    alignItems: 'center', 
    justifyContent: 'center'
  },
  minlayout:{
    flexDirection:"row",
    margin:10,
    
  }
})