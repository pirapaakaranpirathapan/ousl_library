export const Colors = {
    white: '#ffffff',
    black: '#000000',
    lightGray: '#B3B4B6',
    lightyellow:'#ffff80',
    accent: '#FFC231',
    accentRed: '#FB5D2E',
    accentPink: '#F96165',
    amber:"#FFE082",
    darkGreen:"#001a00"
  };
