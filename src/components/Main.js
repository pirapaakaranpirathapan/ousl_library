import { StyleSheet, Text, View, TextInput,ScrollView } from 'react-native'
import React from 'react'
import { Colors } from '../contents/Colors'
import { Card } from 'react-native-paper';
import Cat from './Cat'
import Books from './Books';

const Main = () => {
    return (
        <ScrollView>
            <View>
                
            <View>
                <TextInput style={styles.search} placeholder={'Please type here…'} />
            </View>
            <View style={styles.card}>
                <Card>
                    <Card.Cover source={{ uri: 'https://picsum.photos/700' }} />
                </Card>
            </View>
            <Text style={{fontSize:20,margin:10}}>தினம் ஒரு திருக்குறள்:</Text>
            <Text style={{fontWeight:"700",color:"black"}}>நயன்ஈன்று நன்றி பயக்கும் பயன்ஈன்று
பண்பின் தலைப்பிரியாச் சொல்.</Text>




            <Text style={{textAlign:"center"}}> பிறர்க்கு நன்மையான பயனைத் தந்து நல்ல பண்பிலிருந்து நீங்காத சொற்கள்,வழங்குவோனுக்கும் இன்பம் தந்து நன்மை பயக்கும்.பிறர்க்கு நன்மையைத் தந்து, இனிய பண்பிலிருந்து விலகாத சொல், இம்மைக்கு உலகத்தாரோடு ஒற்றுமையையும், மறுமைக்கு அறத்தையும் கொடுக்கும்.'</Text>
            <Text style={{margin:20}}> Select Your Category </Text>
            <ScrollView horizontal={true} >
            <Books title="Children" body="18 < Books" img="https://www.coe.int/documents/9577501/0/ECHR+Factsheet+children_2021/0cb5d3f9-c74d-4450-add0-84a9fcee77bb" />
            <Books title="Adventure" body="Adventure" img="https://scth.scene7.com/is/image/scth/Adventure_Activities_Hero_Banner:crop-375x280?defaultImage=Adventure_Activities_Hero_Banner&wid=375&hei=280" />
            <Books title="Romance" body="Romance" img="https://img.buzzfeed.com/buzzfeed-static/static/2020-10/7/17/asset/b41e5b39c626/sub-buzz-420-1602093191-29.jpg" />
            
            </ScrollView>
            <Cat />
            
        </View>
        </ScrollView>
    )
}

export default Main

const styles = StyleSheet.create({
    layout: {
        backgroundColor: Colors.white,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    search: {
        backgroundColor: "pink",
        color: "white",
        margin: 5,
        borderRadius: 10
    },
    card:{
        margin:5
    }

})