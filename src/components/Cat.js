import { StyleSheet, Text, View,ScrollView } from 'react-native'
import React from 'react'
import BooksCard from './BooksCard'
const Cat = () => {
  return (
    <View>
      <ScrollView >
      
            <BooksCard title="தமிழ்க் காப்பியங்கள்" body="ஆசிரியர் : ஜகந்நாதன், கி. வா." img="https://www.tamildigitallibrary.in/admin/assets/cover/TVA_BOK_0026712_%20%E0%AE%9C%E0%AE%95%E0%AE%A8%E0%AF%8D%E0%AE%A8%E0%AE%BE%E0%AE%A4%E0%AE%A9%E0%AF%8D_%E0%AE%95%E0%AE%BF_%E0%AE%B5%E0%AE%BE.jpg" price="500rs" />
            <BooksCard title="எழு பெரு வள்ளல்கள்" body="ஆசிரியர் : ஜகந்நாதன், கி. வா." img="https://www.tamildigitallibrary.in/admin/assets/cover/TVA_BOK_0026700_%E0%AE%9C%E0%AE%95%E0%AE%A8%E0%AF%8D%E0%AE%A8%E0%AE%BE%E0%AE%A4%E0%AE%A9%E0%AF%8D_%E0%AE%95%E0%AE%BF_%E0%AE%B5%E0%AE%BE.jpg" price="300rs"/>
            <BooksCard title="சாதி ஒழிப்பு" body="ஆசிரியர்.அம்பேத்கர், பி. ஆர்." img="https://www.tamildigitallibrary.in/admin/assets/cover/TVA_BOK_0028173_%E0%AE%9A%E0%AE%BE%E0%AE%A4%E0%AE%BF_%E0%AE%92%E0%AE%B4%E0%AE%BF%E0%AE%AA%E0%AF%8D%E0%AE%AA%E0%AF%81.jpg" price="200rs"/>  

      </ScrollView>
      
    </View>
  )
}

export default Cat

const styles = StyleSheet.create({})