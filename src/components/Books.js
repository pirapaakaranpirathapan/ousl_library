import * as React from 'react';
import { Avatar, Button, Card, Title, Paragraph } from 'react-native-paper';
import {StyleSheet,View} from'react-native'


const MyComponent = (props) => (
  <View >
      <Card  >
    
    <Card.Content>
    <Card.Cover source={{ uri: props.img }} style={{width:200,backgroundColor:"pink",padding:5,borderRadius:20}} />
      <Title style={{textAlign:"center"}}>{props.title}</Title>
      <Paragraph style={{textAlign:"center"}}>{props.body}</Paragraph>
      
    </Card.Content>
    
   
  </Card>
  </View>
);

export default MyComponent;

const styles = StyleSheet.create({
    
    

})