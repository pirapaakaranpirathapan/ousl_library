import * as React from 'react';
import { Avatar, Card, Title, Paragraph } from 'react-native-paper';
import {StyleSheet,View,Button,Alert} from'react-native'

const more = (data) =>
    Alert.alert(
      "தமிழ்க் காப்பியங்கள்",
      "ஆசிரியர் : ஜகந்நாதன், கி. வா.",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Buy Now", onPress: () => console.log("OK Pressed") }
      ]
    );


const MyComponent = (props) => (

  
  <View>
      <Card style={styles.layout}>
    
    <Card.Content>
    <Card.Cover source={{ uri: props.img }} />
      <Title>{props.title}</Title>
      <Paragraph>{props.body}</Paragraph>
      <Title>Price:{props.price}</Title>
    </Card.Content>
    <Button title='ReadMore...' onPress={more} />
    
       
   
  </Card>
  </View>
);

export default MyComponent;

const styles = StyleSheet.create({
    layout: {
        backgroundColor: 'pink',
        margin:10,
        borderRadius:10
        
    },
    

})